var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

var asset_head = new Image();
asset_head.src = "assets/viper_head.svg"
var asset_straight = new Image();
asset_straight.src = "assets/viper_straight_body.svg"



var drawModule = (function () {
  var init = function() {
    console.log('Starting Game');
    ctx.drawImage(asset_head, 50,5);
    ctx.save();
    ctx.rotate(100);
    ctx.drawImage(asset_straight, 100,100);
    ctx.restore();
  };

  var drawHead = function() {

  }

  return {
    init: init
  };
}());

(function (window, document, drawModule, undefined) {
  var btn = document.getElementById('start');
  btn.addEventListener("click", function () {
      drawModule.init();
  });

  document.onkeydown = function (event) {
    code = event.keyCode;

    switch (code) {
      case 37:
        console.log('lets go left');
        break;
      case 38:
        console.log('lets go up')
        break;
      case 39:
        console.log('lets go right')
        break;
      case 40:
        console.log('lets go down')
        break;
    }
  }
})(window, document, drawModule);
